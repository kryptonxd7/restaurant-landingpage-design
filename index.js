window.addEventListener('scroll', function(){
    const headerEl = document.querySelector("header");
    headerEl.classList.toggle('sticky', window.scrollY > 0);
});

function toggleMenu() {
    const menuToggle = document.querySelector('.menuToggle');
    const navToggle = document.querySelector('.nav-list');
    menuToggle.classList.toggle('active');
    navToggle.classList.toggle('active');
}

